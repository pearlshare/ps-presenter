# Presenter

Render objects in a standardized way from express.

## Features

Handles:

* Pagination of collections through HTTP headers
* Render errors
* Render HTML/JSON objects based on Accepts headers
* Render HTML/JSON collections based on Accepts headers

## Usage

```coffee
  Presenter = require("ps-presenter")

  app.get 'item/:itemId', (req, res) ->
    presenter = new Presenter(req, res, defaultContentType: 'application/json')

    Item.findOne param('itemId'), (err, item) ->
      if err
        presenter.renderErr(err)
      else
        presenter.renderOne(item)

  app.get 'items', (req, res) ->
    presenter = new Presenter(req, res, defaultContentType: 'application/json')

    Item.findAll (err, items) ->
      if err
        presenter.renderErr(err)
      else
        presenter.renderCollection(items)

```

### License

MIT