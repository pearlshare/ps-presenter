queryString = require("querystring")

class Presenter

  constructor: (req, res, options = {}) ->
    @req = req
    @res = res

    @urlPrefix = options.urlPrefix || ''

    # Setup paging
    @page = parseInt(if parseInt(req.param("page")) > 0 then req.param("page") else 1)
    @perPage = req.param("perPage") || options.perPage || 50

    # Setup default request type
    if options.defaultContentType
      @req.headers['content-type'] = options.defaultContentType unless @req.get("content-type")?

    # Add default request structure
    @resultSets = {} # {pearls: 20, collections: 10}
    @meta = {}
    @response = []

  #
  # Helpers
  #
  pageOffset: ->
    (@page - 1) * @perPage

  nextPageOffset: ->
    @page * @perPage

  hasNextPage: ->
    for set, count of @resultSets
      return true if @nextPageOffset() < count


  #
  # Builder
  #

  ###
    Add an item to the response
    
    @param {Object} item - the item to add to the response
  ###
  addItem: (item) ->
    @response.push item


  #
  # Renderers
  #

  ###
    renderErr - render an error object
    @param {Object} err
  ###
  renderErr: (err, options = {}) =>

    options.status ||= 400
    if typeof(err) == 'Object' and err.constructor.prototype == 'Error'
      err = new Error(err)
      
    if @req.accepts('application/json')
      @res.json options.status, errors: err
    else
      @res.render(options.status) if err


  ###
    renderOne - render a single item
    @param {Object} item
  ###
  renderOne: (item) =>
    if @req.accepts('application/json')
      @renderOneJSON(item)
    else
      @renderOneHTML(item)


  ###
    renderOneJSON - render a single JSON item
    @param {Object} item - JS Object to render as JSON
  ###
  renderOneJSON: (item) ->
    if item
      @res.json item
    else
      @res.json 404, {}


  ###
    renderOneHTML - renders a single html item view
    @param {Object} item - passed to the view
  ###
  renderOneHTML: (item) ->
    @res.render "#{item.constructor.modelName}/show", item: item


  ###
    Default render collection action

    Renders JSON or HTML based on request content-type headers
    @param {Array} collection - array of items to render
  ###
  renderCollection: (collection) =>
    # Set pagination headers
    @res.setHeader 'pagination.page', @page
    @res.setHeader 'pagination.perPage', @perPage

    if rslt = /(.*)\?.*$/.exec(@req.originalUrl)
      baseUrl = rslt[1]

    baseUrl ||= @req.originalUrl

    # Set link headers
    links = {}
    # next link
    if @hasNextPage()
      nextParams =
        page: @page + 1
        perPage: @perPage
      for key, value of @req.query
        nextParams[key] ||= value
      links.next = baseUrl + "?" + queryString.stringify(nextParams)

    # prev link
    if @page > 1
      prevParams =
        page: @page - 1
        perPage: @perPage
      for key, value of @req.query
        prevParams[key] ||= value
      links.prev = baseUrl + "?" + queryString.stringify(prevParams)

    @res.links links

    if @req.accepts('application/json')
      @renderIndexJSON(collection)
    else
      @renderIndexHTML(collection)


  ###
    Create the standard collection JSON formatted output
    @returns {Promise} 
  ###
  renderIndexJSON: (collection) ->
    for key, value of @meta
      @res.setHeader key, value.toJSON()
    
    if collection.length > 0
      for item in collection
        @addItem(item) if Object.keys(item).length > 0
      
      @res.json @response
    else
      @res.json 200, []


  ###
    Create the standard collection HTML formatted output
    @param {Array} collection - items to render
  ###
  renderIndexHTML: (collection) ->
    @res.render "#{item.constructor.modelName}/index", collection: collection


module.exports = Presenter